import React from 'react'
import { Route, Link } from 'react-router-dom'
import Home from '../home'
import About from '../about'
import MovieList from '../movielist'

const App = () => (
  <div>
    <header style={{ width: '80%', display: 'flex', justifyContent: 'space-between', margin: ' 30px auto'}}>
      <Link to="/">Home</Link>
      <Link to="/about-us">About</Link>
      <Link to="/movie-list">MovieList</Link>
    </header>

    <main>
      <Route exact path="/" component={Home} />
      <Route exact path="/about-us" component={About} />
      <Route exact path="/about-us" component={About} />
      <Route exact path="/movie-list" component={MovieList} />
    </main>
  </div>
)

export default App
