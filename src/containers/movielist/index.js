import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { addToCart } from '../../modules/cart'

const movies = [
  { id: 1, title: 'Harry Puttar', descriptions: 'Wizard Guy with a scar' },
  { id: 2, title: 'Golmaal', descriptions: 'AEIOU' },
  { id: 2, title: 'Munna Bhai', descriptions: 'Sabka Bhai' },
]

export const index = ({ dispatchAddToCart, cart }) => {
  // const cartList = cart.cartList;
  const { cartList } = cart
  // const dispatchAddToCart = props.dispatchAddToCart
  return (
    <div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center',
          width: '80%',
          margin: '20px auto',
          background: 'wheat',
        }}>
        <div>
          <h1>Nav Bar</h1>
        </div>
        <div>Cart {cartList}</div>
      </div>

      <section style={{ margin: '20px 100px', width: '80%', flexWrap: 'wrap' }}>
        {movies.map((arr) => (
          <div
            key={arr.id}
            style={{
              margin: '10px',
              border: '1px solid black',
              width: '300px',
            }}>
            <div>
              <p style={{ textAlign: 'center' }}>Title: {arr.title}</p>
            </div>
            <p style={{ textAlign: 'center' }}>
              Descriptions: {arr.descriptions}
            </p>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <button
                style={{ margin: '20px' }}
                onClick={() => {
                  console.log('On CLikc button triggered')

                  // Dispact an action - addToCart
                  dispatchAddToCart(arr)
                }}>
                Add to cart
              </button>
            </div>
          </div>
        ))}
      </section>
    </div>
  )
}

// Map my redux state/store to components props
const mapStateToProps = (state) => ({
  cart: state.cart,
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      dispatchAddToCart: addToCart,
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(index)
