// export const INCREMENT_REQUESTED = 'cart/INCREMENT_REQUESTED'
export const ADD_TO_CART = 'cart/ADD_TO_CART'

const initialState = {
  cartList: 0,
  selectedMovie: [],
}

export default (state = initialState, action) => {
  console.log('before switch state', state.selectedMovie, action.payload)
  switch (action.type) {
    // case INCREMENT_REQUESTED:
    //   return {
    //     ...state,
    //     isIncrementing: true,
    //   }

    case ADD_TO_CART:
      return {
        ...state,
        cartList: state.cartList + 1,
        selectedMovie: [...state.selectedMovie, action.payload],
      }

    default:
      return state
  }
}

// export const increment = () => {
//   return (dispatch) => {
//     dispatch({
//       type: INCREMENT_REQUESTED,
//     })

//     dispatch({
//       type: INCREMENT,
//     })
//   }
// }

export const addToCart = (payload) => {
  console.log('addToCart inside action definition')
  return (dispatch) => {
    dispatch({
      type: ADD_TO_CART,
      payload,
    })
  }
}
