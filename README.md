## React Training Session

- Medium Link: [Please find the link for the medium article on redux](https://medium.com/@bretcameron/a-beginners-guide-to-redux-with-react-50309ae09a14)

- Presentation Link: [Please find the link for the presentation](https://lucid.app/lucidchart/invitations/accept/inv_7d75971b-199c-44e6-a81c-9e3f056bc085?viewport_loc=656%2C1374%2C1819%2C861%2C0_0)
